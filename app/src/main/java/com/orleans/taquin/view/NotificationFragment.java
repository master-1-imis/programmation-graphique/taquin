package com.orleans.taquin.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.orleans.taquin.R;


public class NotificationFragment extends DialogFragment {

	// ATTRIBUTE --------------------------------------------------------------
	private boolean winner;
	private String time;


	// CONSTRUCTOR ------------------------------------------------------------
	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final View root = LayoutInflater.from(this.getActivity()).inflate(R.layout.fragment_notification, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setView(root);

		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		// afficher si l'utilisateur à gagné ou perdu
		TextView result = (TextView) root.findViewById(R.id.result);
		if (this.winner) { result.setText(R.string.winner); }
		else { result.setText(R.string.loser); }

		// afficher le temps de jeu
		TextView chronometer = (TextView) root.findViewById(R.id.chronometer);
		chronometer.setText(this.time);

		Button back = (Button) root.findViewById(R.id.button);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().finish();
			}
		});

		return dialog;
	}


	// METHOD -----------------------------------------------------------------
	public void setWinner(boolean isWinner) {
		this.winner = isWinner;
	}


	public void setTime(int time) {
		if (this.winner) {
			String secondes = Integer.toString(time % 60) + "s";
			String minutes = (time / 60) % 60 == 0 ? "" : Integer.toString((time / 60) % 60) + "min ";
			this.time = "Terminé en : " + minutes + secondes;
		}
		else {
			this.time = "Dommage \uD83D\uDE08";
		}
	}
}