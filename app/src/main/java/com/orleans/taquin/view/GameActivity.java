package com.orleans.taquin.view;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Chronometer;

import androidx.appcompat.app.AppCompatActivity;

import com.orleans.taquin.MainActivity;
import com.orleans.taquin.R;
import com.orleans.taquin.opengl.MyGLSurfaceView;


public class GameActivity extends AppCompatActivity {

	// ATTRIBUTE --------------------------------------------------------------
	private MyGLSurfaceView surfaceView;
	private Chronometer chronometer;
	private NotificationFragment notification;
	private int counter;
	private int base;


	// CONSTRUCTOR ------------------------------------------------------------
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// récupérer les paramètres
		Intent intent = getIntent();
		int line_size = intent.getIntExtra(MainActivity.LINE_SIZE, 3);

		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		// initialisation de la vue
		setContentView(R.layout.activity_game);
		this.surfaceView = this.findViewById(R.id.surfaceView);
		this.surfaceView.init(line_size,v);

		// définir le chronomètre
		this.base = 60;
		switch (line_size) {
			case 3: this.base *= 2; break;
			case 4: this.base *= 5; break;
			case 5: this.base *= 10; break;
		}
		this.counter = this.base;
		this.chronometer = this.findViewById(R.id.chronometer);
		this.chronometer.setText(formatCounter(this.counter));
		this.chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
			@Override
			public void onChronometerTick(Chronometer chronometer) {
				onChronometerTichHandler();
			}
		});

		// initialiser la pop-up
		this.notification = new NotificationFragment();
	}


	// METHOD -----------------------------------------------------------------
	private void onChronometerTichHandler() {
		if (this.counter <= 0 || this.surfaceView.isCombination_finded()) {
			this.chronometer.stop();
			this.surfaceView.stopPlaying();

			if (this.counter >= 0 && this.surfaceView.isCombination_finded()) { this.notification.setWinner(true); }
			else { this.notification.setWinner(false); }

			this.notification.setTime(this.base - this.counter);

			// afficher la pop-up
			this.notification.show(this.getSupportFragmentManager(), "notification");
		}
		else {
			this.counter--;
		}
		this.chronometer.setText(this.formatCounter(this.counter));
	}


	private String formatCounter(int time) {
		String secondes = Integer.toString(time % 60);
		String minutes = Integer.toString((time / 60) % 60);
		secondes = secondes.length() == 1 ? "0" + secondes : secondes;
		minutes = minutes.length() == 1 ? "0" + minutes : minutes;
		return minutes + ":" + secondes;
	}


	// EVENT ------------------------------------------------------------------
	public void start(View view) {
		this.chronometer.start();
		this.surfaceView.mixeGrid();
		view.setVisibility(View.INVISIBLE);
	}


	// NAVIGATION -------------------------------------------------------------
	@Override
	public void finish() {
		super.finish();
		this.chronometer.stop();
		this.overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
	}
}