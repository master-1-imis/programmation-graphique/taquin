package com.orleans.taquin.opengl;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square {

	// ATTRIBUTE --------------------------------------------------------------
	private final String vertexShaderCode = "#version 300 es\n" +
			"uniform mat4 uMVPMatrix;\n" +
			"in vec3 vPosition;\n" +
			"in vec4 vCouleur;\n" +
			"out vec4 Couleur;\n" +
			"out vec3 Position;\n" +
			"void main() {\n" +
				"Position = vPosition;\n" +
				"gl_Position = uMVPMatrix * vec4(vPosition,1.0);\n" +
				"Couleur = vCouleur;\n" +
			"}\n";

	private final String fragmentShaderCode = "#version 300 es\n" +
			"precision mediump float;\n" +
			"in vec4 Couleur;\n" +
			"in vec3 Position;\n" +
			"out vec4 fragColor;\n" +
			"void main() {\n" +
				"float x = Position.x;\n" +
				"float y = Position.y;\n" +
				"//float test = x*x+y*y;\n" +
				"//if (test>1.0) \n" +
				"//discard;\n" +
				"fragColor = Couleur;\n" +
			"}\n";

	/* les déclarations pour l'équivalent des VBO */
	private final FloatBuffer vertexBuffer;                     // Buffer des coordonnées des sommets du carré
	private final ShortBuffer indiceBuffer;                     // Buffer des indices
	private final FloatBuffer colorBuffer;                      // Buffer des couleurs des sommets

	public int getIdProgram() {
		return idProgram;
	}

	/* les déclarations pour les shaders */
	private final int idProgram;                                // identifiant du programme pour lier les shaders
	private int idPosition;                                     // idendifiant (location) pour transmettre les coordonnées au vertex shader
	private int idCouleur;                                      // identifiant (location) pour transmettre les couleurs
	private int idMatrixMVP;                                    // identifiant (location) pour transmettre la matrice PxVxM

	static final int COORDS_PER_VERTEX = 3;                     // nombre de coordonnées par vertex
	static final int COULEURS_PER_VERTEX = 4;                   // nombre de composantes couleur par vertex

	private int[] linkStatus = {0};

	private float coords[];                                     // tableau de coordonées des points
	private float colors[];                                     // tableau de couleur
	private float position[];                                   // tableau de position
	private short indices[];                                    // tableau d'indice

	private final int vertexStride = COORDS_PER_VERTEX * 4;     // le pas entre 2 sommets : 4 octets par vertex
	private final int couleurStride = COULEURS_PER_VERTEX * 4;  // le pas entre 2 couleurs : 4 octets par vertex


	// CONSTRUCTOR ------------------------------------------------------------
	public Square(float[] position, float[] coords, short[] indices, float[] colors) {

		this.position = position;   // initialiser la position
		this.coords = coords;       // initialiser la taille
		this.indices = indices;     // initialiser les indices
		this.colors = colors;       // initialiser la couleur

		// initialisation du buffer pour les vertex (4 octets par float)
		ByteBuffer bb = ByteBuffer.allocateDirect(this.coords.length * 4);
		bb.order(ByteOrder.nativeOrder());
		this.vertexBuffer = bb.asFloatBuffer();
		this.vertexBuffer.put(this.coords);
		this.vertexBuffer.position(0);

		// initialisation du buffer pour les couleurs (4 octets par float)
		ByteBuffer bc = ByteBuffer.allocateDirect(this.colors.length * 4);
		bc.order(ByteOrder.nativeOrder());
		this.colorBuffer = bc.asFloatBuffer();
		this.colorBuffer.put(this.colors);
		this.colorBuffer.position(0);

		// initialisation du buffer des indices
		ByteBuffer dlb = ByteBuffer.allocateDirect(this.indices.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		this.indiceBuffer = dlb.asShortBuffer();
		this.indiceBuffer.put(this.indices);
		this.indiceBuffer.position(0);

		// chargement des shaders
		int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER, this.vertexShaderCode);
		int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, this.fragmentShaderCode);

		this.idProgram = GLES20.glCreateProgram();              // create empty OpenGL Program
		GLES20.glAttachShader(this.idProgram, vertexShader);    // add the vertex shader to program
		GLES20.glAttachShader(this.idProgram, fragmentShader);  // add the fragment shader to program
		GLES20.glLinkProgram(this.idProgram);                   // create OpenGL program executables
		GLES20.glGetProgramiv(this.idProgram, GLES20.GL_LINK_STATUS, linkStatus,0);
	}


	// METHOD -----------------------------------------------------------------
	public void draw(float[] matrixMVP) {
		// Add program to OpenGL environment
		GLES20.glUseProgram(this.idProgram);

		// récuprer l'id de gestion de la matrice, et des VBO
		this.idMatrixMVP = GLES20.glGetUniformLocation(this.idProgram, "uMVPMatrix");
		this.idPosition = GLES20.glGetAttribLocation(this.idProgram, "vPosition");
		this.idCouleur = GLES20.glGetAttribLocation(this.idProgram, "vCouleur");

		// appliquer la projection et la transformation
		GLES20.glUniformMatrix4fv(this.idMatrixMVP, 1, false, matrixMVP, 0);

		// activer des buffers
		GLES20.glEnableVertexAttribArray(this.idPosition);
		GLES20.glEnableVertexAttribArray(this.idCouleur);

		// lire les buffers
		GLES20.glVertexAttribPointer(this.idPosition, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, this.vertexStride, this.vertexBuffer);
		GLES20.glVertexAttribPointer(this.idCouleur, COULEURS_PER_VERTEX, GLES20.GL_FLOAT, false, this.couleurStride, this.colorBuffer);

		// déssiner l'objet
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, this.indices.length, GLES20.GL_UNSIGNED_SHORT, this.indiceBuffer);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(this.idPosition);
		GLES20.glDisableVertexAttribArray(this.idCouleur);
	}
}
