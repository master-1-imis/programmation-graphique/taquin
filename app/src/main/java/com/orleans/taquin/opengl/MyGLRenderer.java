package com.orleans.taquin.opengl;


import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGLRenderer implements GLSurfaceView.Renderer {

	// ATTRIBUTE --------------------------------------------------------------
	private final int DEFAULT_LINE_SIZE = 3;
	public static final float DEFAULT_SIZE_PROJECTION_X = 10.0f;
	public static final float DEFAULT_SIZE_PROJECTION_Y = 10.0f;
	public static float SIZE_PROJECTION_X = DEFAULT_SIZE_PROJECTION_X;
	public static float SIZE_PROJECTION_Y = DEFAULT_SIZE_PROJECTION_Y;
	public static final float STEP_PROJECTION = 3f;

	private Map<Integer, Square> squares;
	private Map<Integer, float[]> positions;
	private List<Integer> squares_ids;

	private int line_size = DEFAULT_LINE_SIZE;
	private boolean playing;
	private boolean combination_finded;

	private final float[] matrix_model = new float[16];
	private final float[] matrix_view = new float[16];
	private final float[] matrix_projection = new float[16];
	private final float[] matrix_mvp = new float[16];


	// CONSTRUCTOR ------------------------------------------------------------
	public MyGLRenderer(int line_size) {
		this.line_size = line_size;
		this.playing = false;
		this.combination_finded = false;

		this.squares = new HashMap<>();
		this.positions = new HashMap<>();

		SIZE_PROJECTION_X = DEFAULT_SIZE_PROJECTION_X + (this.line_size - DEFAULT_LINE_SIZE) * STEP_PROJECTION;
		SIZE_PROJECTION_Y = DEFAULT_SIZE_PROJECTION_Y + (this.line_size - DEFAULT_LINE_SIZE) * STEP_PROJECTION;
	}


	// METHOD -----------------------------------------------------------------
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// la couleur du fond d'écran
		GLES20.glClearColor(Colors.LIGHT, Colors.LIGHT, Colors.LIGHT, 1.0f);

		float limit = SIZE_PROJECTION_X - STEP_PROJECTION - 1;
		float step = STEP_PROJECTION * 2;

		int base = (int) (Math.random() * Colors.PALLET.length);

		// créer les cases
		for (int square_id = 0; square_id < Math.pow(this.line_size, 2); square_id++) {
			int line = (int) Math.floor((float) square_id / this.line_size);
			int column = square_id % this.line_size;

			float[] position = {-limit + column * step, -limit + line * step};
			float[] coords = Coords.COORDS_SQUARE_ALL[this.line_size - line - 1];
			short[] indices = Coords.INDICES_SQUARE_ALL[this.line_size - line - 1];

			Square square = new Square(position, coords, indices, Colors.color(base, column, coords.length));

			this.squares.put(square_id, square);
			this.positions.put(square_id, position);
		}

		// On retire le carre en bas à droite
		int empty_id = this.line_size-1;
		this.squares.put(empty_id, null);

		//sauvegarder l'ordre initiale des carrés
		this.squares_ids = new ArrayList<>();
		for (int i : this.squares.keySet()) {
			Square s = this.squares.get(i);
			int id=0;
			if (s!=null)
				id=s.getIdProgram();
			this.squares_ids.add(id);
		}
	}


	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// définir la dimention du rendus 2D et centré sur l'écran
		GLES20.glViewport(0, 0, width, height);

		float smaller = Math.min(width, height);
		float ratio = (float) Math.max(width, height) / smaller;

		float left = smaller == width ? -SIZE_PROJECTION_X : -SIZE_PROJECTION_X * ratio;
		float right = smaller == width ? SIZE_PROJECTION_X : SIZE_PROJECTION_X * ratio;
		float top = smaller == height ? SIZE_PROJECTION_Y : SIZE_PROJECTION_Y * ratio;
		float bottom = smaller == height ? -SIZE_PROJECTION_Y : -SIZE_PROJECTION_Y * ratio;

		SIZE_PROJECTION_X = right;
		SIZE_PROJECTION_Y = top;

		// définir les axes de projection orthonormé
		Matrix.orthoM(this.matrix_projection, 0, left, right, bottom, top, -1.0f, 1.0f);
	}


	@Override
	public void onDrawFrame(GL10 gl) {
		// vider le buffer de couleur et de profondeur
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

		for (int i : this.squares.keySet()) {
			Square square = this.squares.get(i);
			float[] position = this.positions.get(i);

			if (square != null && position != null) {
				float[] scratch = new float[16]; // pour stocker une matrice

				// définir le décalage par rapport à la vue de la caméra
				Matrix.setIdentityM(this.matrix_view, 0);

				// calculer la projection et la transformation de la vue
				Matrix.multiplyMM(this.matrix_mvp, 0, this.matrix_projection, 0, this.matrix_view, 0);
				Matrix.setIdentityM(this.matrix_model, 0);

				// définir la position initiale de l'objet Square
				Matrix.translateM(this.matrix_model, 0, position[0], position[1], 0);
//				Log.d("debug", "square: (" + position[0] + ";" + position[1] + ")");

				// calcul de la matrice finale
				Matrix.multiplyMM(scratch, 0, this.matrix_mvp, 0, this.matrix_model, 0);

				// dessiner le carré
				square.draw(scratch);
			}
		}
	}


	// La gestion des shaders
	public static int loadShader(int type, String shaderCode){
		int shader = GLES20.glCreateShader(type);

		// add the source code to the shader and compile it
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);

		return shader;
	}


	public Square getSquare(int square_id) {
		return this.squares.get(square_id);
	}


	public void switchPosition(int square_id, int empty_id) {
		this.squares = Game.swapSquare(this.squares, square_id, empty_id);
		if(Game.checkVictory(this.squares,this.squares_ids)){
			this.stopPlaying();
			this.combination_finded = true;
		}
	}


	public void mixeGrid() {
		// mélanger les carrées
		this.squares = Game.mixGrid(this.squares, (int)Math.pow(line_size,3));
		this.playing = true;
	}


	public boolean isPlaying() {
		return this.playing;
	}


	public boolean isCombination_finded() {
		return this.combination_finded;
	}


	public void stopPlaying() {
		this.playing = false;
	}
}
