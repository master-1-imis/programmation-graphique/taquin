package com.orleans.taquin.opengl;


public class Colors {

	// ATTRIBUTE --------------------------------------------------------------
	public static final float LIGHT = 0.93f;
	public static int[][] PALLET = {
			{ 229,  57,  53 }, // red
			{ 216,  27,  96 }, // pink
			{ 142,  36, 170 }, // purple
			{  57,  73, 171 }, // deep-purple
			{  30, 136, 229 }, // blue
			{   0, 172, 193 }, // cyan
			{   0, 137, 123 }, // teal
			{  67, 160,  71 }, // green
			{ 124, 179,  66 }, // light-green
			{ 192, 202,  51 }, // lime
			{ 253, 216,  53 }, // yellow
			{ 255, 179,   0 }, // amber
			{ 251, 140,   0 }, // orange
			{ 244,  81,  30 }, // deep-orange
			{ 109,  76,  65 }, // brown
			{  84, 110, 122 }  // blue-grey
	};


	// METHOD -----------------------------------------------------------------
	public static float[] color(int base, int offset, int nb_vertex) {
		int pos = (base + offset) % PALLET.length;

		float red   = PALLET[pos][0] / 255.0f;
		float green = PALLET[pos][1] / 255.0f;
		float blue  = PALLET[pos][2] / 255.0f;
		float alpha = 1.0f;

		float[] colors = new float[nb_vertex * 4];

		for (int i = 0; i < nb_vertex * 4; i += 4) {
			colors[i] = red;
			colors[i+1] = green;
			colors[i+2] = blue;
			colors[i+3] = alpha;
		}

		return colors;
	}
}
