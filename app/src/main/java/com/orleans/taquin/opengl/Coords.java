package com.orleans.taquin.opengl;

public class Coords {

	// ATTRIBUTE --------------------------------------------------------------
	public static final float SIZE_SMALL = 2.5f;

	public static final float[] COORDS_SQUARE_1 = BuildSquare1(SIZE_SMALL);
	public static final float[] COORDS_SQUARE_2 = BuildSquare2(SIZE_SMALL);
	public static final float[] COORDS_SQUARE_3 = BuildSquare3(SIZE_SMALL);
	public static final float[] COORDS_SQUARE_4 = BuildSquare4(SIZE_SMALL);
	public static final float[] COORDS_SQUARE_5 = BuildSquare5(SIZE_SMALL);


	public static final short[] INDICES_SQUARE_1 = {
		0,  1,  9,
		0,  9,  8,
		1,  2,  5,
		1,  5,  4,
		6,  7, 10,
		6, 10,  9,
		2,  3, 11,
		2, 11, 10
	};
	public static final short[] INDICES_SQUARE_2 = {
		 0,  1, 17,
		 0, 17, 16,
		 1,  2,  7,
		 1,  7,  4,
		 5,  6, 11,
		 5, 11, 10,
		 8,  9, 14,
		 8, 14, 13,
		12, 15, 18,
		12, 18, 17,
		 2,  3, 19,
		 2, 19, 18
	};
	public static final short[] INDICES_SQUARE_3 = {
		 0,  1, 17,
		 0, 17, 16,
		 1,  2,  9,
		 1,  9,  4,
		 5,  6, 11,
		 5, 11, 10,
		 7,  8, 13,
		 7, 13, 12,
		14, 15, 18,
		14, 18, 17,
		 2,  3, 19,
		 2, 19, 18
	};
	public static final short[] INDICES_SQUARE_4 = {
		 0,  1, 16,
		 0, 16, 15,
		 1,  2,  6,
		 1,  6,  4,
		 7,  8, 10,
		 7, 10,  9,
		 5,  6, 12,
		 5, 12, 11,
		13, 14, 17,
		13, 17, 16,
		 2,  3, 18,
		 2, 18, 17
	};
	public static final short[] INDICES_SQUARE_5 = {
		 0,  1, 17,
		 0, 17, 16,
		 1,  2,  7,
		 1,  7,  4,
		 8,  9, 14,
		 8, 14, 13,
		 5,  6, 11,
		 5, 11, 10,
		12, 15, 18,
		12, 18, 17,
		 2,  3, 19,
		 2, 19, 18
	};

	public static final float[][] COORDS_SQUARE_ALL = {
		COORDS_SQUARE_1,
		COORDS_SQUARE_2,
		COORDS_SQUARE_3,
		COORDS_SQUARE_4,
		COORDS_SQUARE_5
	};
	public static final short[][] INDICES_SQUARE_ALL = {
		INDICES_SQUARE_1,
		INDICES_SQUARE_2,
		INDICES_SQUARE_3,
		INDICES_SQUARE_4,
		INDICES_SQUARE_5
	};


	// METHOD -----------------------------------------------------------------
	private static float[] BuildSquare1(float size) {
		return new float[] {
			    -1.0f * size,       1.0f * size, 0.0f, // 0
			    -1.0f * size,  5.0f/7.0f * size, 0.0f, // 1
			    -1.0f * size, -5.0f/7.0f * size, 0.0f, // 2
			    -1.0f * size,      -1.0f * size, 0.0f, // 3
			1.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 4
			1.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 5
			3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 6
			3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 7
			     1.0f * size,       1.0f * size, 0.0f, // 8
			     1.0f * size,  5.0f/7.0f * size, 0.0f, // 9
			     1.0f * size, -5.0f/7.0f * size, 0.0f, // 10
			     1.0f * size,      -1.0f * size, 0.0f  // 11
		};
	}


	private static float[] BuildSquare2(float size) {
		return new float[] {
				 -1.0f * size,       1.0f * size, 0.0f, // 0
				 -1.0f * size,  5.0f/7.0f * size, 0.0f, // 1
				 -1.0f * size, -5.0f/7.0f * size, 0.0f, // 2
				 -1.0f * size,      -1.0f * size, 0.0f, // 3
			-3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 4
			-3.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 5
			-3.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 6
			-3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 7
			-1.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 8
			-1.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 9
			 1.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 10
			 1.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 11
			 3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 12
			 3.0f/7.0f* size,  -1.0f/7.0f * size, 0.0f, // 13
			 3.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 14
			 3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 15
			      1.0f * size,       1.0f * size, 0.0f, // 16
			      1.0f * size,  5.0f/7.0f * size, 0.0f, // 17
			      1.0f * size, -5.0f/7.0f * size, 0.0f, // 18
			      1.0f * size,      -1.0f * size, 0.0f  // 19
		};
	}


	private static float[] BuildSquare3(float size) {
		return new float[] {
				 -1.0f * size,       1.0f * size, 0.0f, // 0
				 -1.0f * size,  5.0f/7.0f * size, 0.0f, // 1
				 -1.0f * size, -5.0f/7.0f * size, 0.0f, // 2
				 -1.0f * size,      -1.0f * size, 0.0f, // 3
			-3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 4
			-3.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 5
			-3.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 6
			-3.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 7
			-3.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 8
			-3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 9
			 1.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 10
			 1.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 11
			 1.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 12
			 1.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 13
			 3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 14
			 3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 15
			      1.0f * size,       1.0f * size, 0.0f, // 16
			      1.0f * size,  5.0f/7.0f * size, 0.0f, // 17
			      1.0f * size, -5.0f/7.0f * size, 0.0f, // 18
			      1.0f * size,      -1.0f * size, 0.0f  // 19
		};
	}


	private static float[] BuildSquare4(float size) {
		return new float[] {
				 -1.0f * size,       1.0f * size, 0.0f, // 0
				 -1.0f * size,  5.0f/7.0f * size, 0.0f, // 1
				 -1.0f * size, -5.0f/7.0f * size, 0.0f, // 2
				 -1.0f * size,      -1.0f * size, 0.0f, // 3
			-3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 4
			-3.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 5
			-3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 6
			-1.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 7
			-1.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 8
			 1.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 9
			 1.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 10
			 1.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 11
			 1.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 12
			 3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 13
			 3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 14
			      1.0f * size,       1.0f * size, 0.0f, // 15
				  1.0f * size,  5.0f/7.0f * size, 0.0f, // 16
				  1.0f * size, -5.0f/7.0f * size, 0.0f, // 17
				  1.0f * size,      -1.0f * size, 0.0f  // 18
		};
	}


	private static float[] BuildSquare5(float size) {
		return new float[] {
				 -1.0f * size,       1.0f * size, 0.0f, // 0
				 -1.0f * size,  5.0f/7.0f * size, 0.0f, // 1
				 -1.0f * size, -5.0f/7.0f * size, 0.0f, // 2
				 -1.0f * size,      -1.0f * size, 0.0f, // 3
			-3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 4
			-3.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 5
			-3.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 6
			-3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 7
			-1.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 8
			-1.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 9
			 1.0f/7.0f * size, -1.0f/7.0f * size, 0.0f, // 10
			 1.0f/7.0f * size, -3.0f/7.0f * size, 0.0f, // 11
			 3.0f/7.0f * size,  5.0f/7.0f * size, 0.0f, // 12
			 3.0f/7.0f * size,  3.0f/7.0f * size, 0.0f, // 13
			 3.0f/7.0f * size,  1.0f/7.0f * size, 0.0f, // 14
			 3.0f/7.0f * size, -5.0f/7.0f * size, 0.0f, // 15
			      1.0f * size,       1.0f * size, 0.0f, // 16
				  1.0f * size,  5.0f/7.0f * size, 0.0f, // 17
				  1.0f * size, -5.0f/7.0f * size, 0.0f, // 18
				  1.0f * size,      -1.0f * size, 0.0f  // 19
		};
	}
}
