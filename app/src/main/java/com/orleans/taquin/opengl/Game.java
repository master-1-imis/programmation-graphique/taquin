package com.orleans.taquin.opengl;


import java.util.List;
import java.util.Map;


public class Game {

	// METHOD -----------------------------------------------------------------
	public static Map<Integer, Square> swapSquare(Map<Integer, Square> grid, int square_id, int empty_id) {
		Square square = grid.get(square_id);
		grid.remove(square_id);
		grid.remove(empty_id);
		grid.put(empty_id, square);
		grid.put(square_id, null);
		return grid;
	}


	public static Map<Integer, Square> mixGrid(Map<Integer, Square> grid, int number_of_shift) {
		int line_size = (int) Math.sqrt(grid.size());
		int empty_id = line_size-1;
		int preced = empty_id;
		int cpt = 0;
		while (cpt != number_of_shift) {
			int t = (int) (Math.random() * 4);
			switch (t) {
				case 0:
					// déplacer la cellule vide vers le bas
					if (preced != empty_id - line_size && empty_id >= line_size) {
						grid = swapSquare(grid, empty_id - line_size, empty_id);
						preced = empty_id;
						empty_id = empty_id - line_size;
						cpt++;
					}
					break;
				case 1:
					// déplacer la cellule vide vers le haut
					if (preced != empty_id + line_size && empty_id <= grid.size() - 1 - line_size) {
						grid = swapSquare(grid, empty_id + line_size, empty_id);
						preced = empty_id;
						empty_id = empty_id + line_size;
						cpt++;
					}
					break;
				case 2:
					// déplacer la cellule vide vers la droite
					if (preced != empty_id + 1 && empty_id % line_size != line_size-1) {
						grid = swapSquare(grid, empty_id + 1, empty_id);
						preced = empty_id;
						empty_id++;
						cpt++;
					}
					break;
				case 3:
					// déplacer la cellule vide vers la gauche
					if (preced != empty_id - 1 && empty_id % line_size != 0) {
						grid = swapSquare(grid, empty_id - 1, empty_id);
						preced = empty_id;
						empty_id--;
						cpt++;
					}
					break;
			}
		}
		return grid;
	}


	public static boolean checkVictory(Map<Integer, Square> squares, List<Integer> squares_ids) {
		if (squares.get((int) Math.sqrt(squares.size())-1) != null) return false;

		for (int i : squares.keySet()) {
			Square square = squares.get(i);

			if (square != null && square.getIdProgram() != squares_ids.get(i)) {
				return false;
			}
		}
		return true;
	}
}
