package com.orleans.taquin.opengl;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.List;

public class MyGLSurfaceView extends GLSurfaceView {

	// ATTRIBUTE --------------------------------------------------------------
	private MyGLRenderer renderer;
	private int line_size;
	private Vibrator vibrator;


	// CONSTRUCTOR ------------------------------------------------------------
	public MyGLSurfaceView(Context context) {
		super(context);
	}


	public MyGLSurfaceView(Context context, AttributeSet attributes) {
		super(context, attributes);
	}


	public void init(int line_size, Vibrator vibrator) {
		this.line_size = line_size;
		this.renderer = new MyGLRenderer(this.line_size);

		this.vibrator=vibrator;

		setEGLConfigChooser(8, 8, 8, 8, 16, 0);

		// Création d'un context OpenGLES 3.0
		setEGLContextClientVersion(2);

		// Création du renderer qui va être lié au conteneur View créé
		setRenderer(this.renderer);

		// Option pour indiquer qu'on redessine uniquement si les données changent
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}


	// METHOD -----------------------------------------------------------------
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		if (this.renderer.isPlaying() && e.getAction()==MotionEvent.ACTION_DOWN) {
			// Les coordonnées du point touché sur l'écran (pixel)
			float screen_x = e.getX();
			float screen_y = e.getY();

			// la taille de l'écran en pixels
			float screen_width = getWidth();
			float screen_height = getHeight();

			// conversion des coordonnées pixel en coordonnées OpenGL
			float opengl_x = (MyGLRenderer.SIZE_PROJECTION_X * 2) * (screen_x / screen_width) - MyGLRenderer.SIZE_PROJECTION_X;
			float opengl_y = -(MyGLRenderer.SIZE_PROJECTION_Y * 2) * (screen_y / screen_height) + MyGLRenderer.SIZE_PROJECTION_Y;
			Log.d("debug", "opengl_pos: (x=" + opengl_x + ";y=" + opengl_y + ")");

			float step = MyGLRenderer.STEP_PROJECTION * 2;

			// calculer le centre du carré le plus proche
			float limit = Math.min(MyGLRenderer.SIZE_PROJECTION_X, MyGLRenderer.SIZE_PROJECTION_Y) - MyGLRenderer.STEP_PROJECTION - 1;
			float center_x = -limit - step;
			float center_y = -limit - step;
			boolean finded_x = false;
			boolean finded_y = false;

			for (float i = -limit; i <= limit; i += step) {
				if (Math.abs(opengl_x - i) <= Coords.SIZE_SMALL) { center_x = i; finded_x = true; }
				if (Math.abs(opengl_y - i) <= Coords.SIZE_SMALL) { center_y = i; finded_y = true; }
			}

			// s'assurer de ne pas être en dehors d'un carré
			if (finded_x && finded_y) {

				// déterminer l'id du carré par rapport aux coordonnées
				int square_idx = Math.round((center_x + limit) / step);
				int square_idy = Math.round((center_y + limit) / step);
				int square_id = square_idy * this.line_size + square_idx;
				Log.d("debug", "square_id: " + square_id + " (idx=" + square_idx + ";idy=" + square_idy + ")");

				if (square_id >= 0 && square_id < this.line_size * this.line_size) {

					// récupérer les identifiants des voisins
					List<Integer> neighbors_id = new ArrayList<>();
					if (square_idx > 0) { neighbors_id.add(square_idy * this.line_size + square_idx - 1); }
					if (square_idy > 0) { neighbors_id.add((square_idy - 1) * this.line_size + square_idx); }
					if (square_idx < this.line_size - 1) { neighbors_id.add(square_idy * this.line_size + square_idx + 1); }
					if (square_idy < this.line_size - 1) { neighbors_id.add((square_idy + 1) * this.line_size + square_idx); }

					boolean hasEmptytNeighbor =false;

					// échanger la position du carré avec la cellule vide
					for (int id : neighbors_id) {
						Square square = this.renderer.getSquare(id);
						if (square == null) {
							hasEmptytNeighbor=true;
							this.renderer.switchPosition(square_id, id);
							break;
						}
					}

					// si le coup est valide, on informe le joueur par une courte vibration
					// sinon par une plus importante vibration
					if(hasEmptytNeighbor) { vibrator.vibrate(40); }
					else{ vibrator.vibrate(100); }

					// mettre à jour l'affichage
					this.requestRender();
				}
			}
		}
		return true;
	}


	public void mixeGrid() {
		this.renderer.mixeGrid();
		this.requestRender();
	}


	public void stopPlaying() {
		this.renderer.stopPlaying();
	}


	public boolean isCombination_finded() {
		return this.renderer.isCombination_finded();
	}
}
