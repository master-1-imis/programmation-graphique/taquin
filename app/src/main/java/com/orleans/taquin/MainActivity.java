package com.orleans.taquin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.orleans.taquin.view.GameActivity;

public class MainActivity extends AppCompatActivity {

	// ATTRIBUTE --------------------------------------------------------------
	public static final String LINE_SIZE = "lineSize";


	// CONSTRUCTOR ------------------------------------------------------------
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// initialisation de la vue
		setContentView(R.layout.activity_main);
	}


	// METHOD -----------------------------------------------------------------
	// none


	// EVENT ------------------------------------------------------------------
	public void levelEasy(View view) {
		goToGameActivity(3);
	}


	public void levelMiddle(View view) {
		goToGameActivity(4);
	}


	public void levelHard(View view) {
		goToGameActivity(5);
	}


	// NAVIGATION -------------------------------------------------------------
	public void goToGameActivity(int lineSize) {
		Intent intent = new Intent(this, GameActivity.class);
		intent.putExtra(LINE_SIZE, lineSize);
		this.startActivityForResult(intent, 0);
		this.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
	}
}