# Taquin

**date :** 15 avril 2021 au 18 mai 2021  
**sujets :** [sujet.pdf](sujet.pdf)    
**langage :** Android  

---

<br/><br/>

## Table des matières
[[_TOC_]]

<br/><br/>

## Objectif
Ce projet s'inscrit dans le cadre d'un module de développement graphique en OpenGL. Il permet de mettre en pratique les connaissances aquises à travers un système ambarqué.

<br/><br/>

## Projet
### Spécificités
**Plateforme :** Android  
**Sdk minimum:** 25  
**Sdk ciblé:** 30  

### Appareils utilisés
| Marque | Modèle | Émulateur | Version Android | Dimensions (w×h) |
| :-- | :-- | :--: | --: | --: |
| Samsung | Galaxy A50 | non | 10 (sdk 29) | 1080×2340 |
| Google | Pixel 3A | oui | 11 (sdk 30) | 1080×2220 |
| Wiko | Y80 | oui | 9 (sdk 28) | 720×1344 |

### Fonctionnalitées
#### Cahier des charges
- [x] Afficher la solution avant le début du jeu
- [x] Lancement du jeu lors de la première intéraction utilisateur
- [x] Génération d'une grille aléatoire (mélangé)
- [x] Déplacement des cases lors d'un clic
- [x] Signalement d'un mouvement invalide
- [x] Signalement de la fin du jeu (gagné ou perdu)

### Bonus
- [x] Chronomètre (décompte)
- [ ] Utilisation de texture
- [x] Taille de grille différentes: mode facile (3×3), mode normale (4×4) et mode difficile (5×5)


<br/><br/>

## Guide d'utilisation
1. Sélectionner le mode de jeu (facile, normale ou difficile)
2. Cliquer sur le bouton "Démarer" pour lancer le jeu et activer le décompte
3. Cliquer sur les cases à côté de la cellule vide pour les déplacer et retrouver la configuration initiale.
	- Un déplacement authorisé sera signalé par une très courte vibration
	- Un déplacement non authorisé sera signalé par un vibration plpus importante
4. Affichage du résultat à la fin du décompte ou lorsque le puzzle est résolu
	- Affichage du temps de résolution en cas de victoire
5. Cliquer sur le bouton "Retour au menu" pour quitter la partie et retourner au menu

<br/>

![captures d'écran](img/plaquette.png)
Voir les captures d'écran dans le répertoire [img](img/).

<br/><br/>

## Prérequis Logiciel
| Logiciel | Version utilisé | Description |
| :-- | :-: | :-- |
| Java | 11 | Langae de développement pour la plateforme Android. |
| OpenGl ES | 2.0 | Librairie pour l'OpenGL les applications nomades. |

<br/><br/>

## Contributeurs  
- Arnaud ORLAY (@arnorlay) : Master 1 IMIS Informatique
- Vincent CAQUELARD (@VincentCaquelard) : Master 1 IMIS Informatique
